package com.javagda19.portal_wiedzy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PortalWiedzyApplication {

    public static void main(String[] args) {
        SpringApplication.run(PortalWiedzyApplication.class, args);
    }

}
