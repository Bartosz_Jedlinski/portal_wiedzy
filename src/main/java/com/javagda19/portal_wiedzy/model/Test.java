package com.javagda19.portal_wiedzy.model;


import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class Test {
@Id
@GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

@OneToOne
  private Result result;

@ManyToMany
    private Set<Question> questions;
@ManyToOne
    private User user;
}
