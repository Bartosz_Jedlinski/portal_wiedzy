package com.javagda19.portal_wiedzy.model;


import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private  String login;
    private String password;
    @Enumerated(EnumType.ORDINAL)
    private TypeUser type_user;
}
