package com.javagda19.portal_wiedzy.model;

public enum TypeUser {
    USER,
    ADMIN,
    SUPERADMIN;
}
